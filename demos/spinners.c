/*
 *  This file is part of XForms.
 *
 *  XForms is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 2.1, or
 *  (at your option) any later version.
 *
 *  XForms is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with XForms; see the file COPYING.  If not, write to
 *  the Free Software Foundation, 59 Temple Place - Suite 330, Boston,
 *  MA 02111-1307, USA.
 */


/*
 * Spinner demo
 *
 *  This file is part of xforms package
 *  Sirio Bolaños Puchet
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "include/forms.h"
#include "fd/fd_spinners_gui.h"
#include <stdlib.h>


/***************************************
 ***************************************/

int
main( int    argc,
      char * argv[ ] )
{
    FD_demo34 *fd_demo34;

    fl_set_border_width( -2 );
    fl_initialize( &argc, argv, 0, 0, 0 );
    fd_demo34 = create_form_demo34( );

    /* show the first form */

    fl_show_form( fd_demo34->demo34, FL_PLACE_CENTER, FL_FULLBORDER,
                  "Spinners demo" );

    while ( fl_do_forms( ) )
        /* empty */ ;

   return 0;
}
